
FROM thinkwhere/gdal-python:latest

# where your code lives
WORKDIR /usr/src/seiketsu

# copy whole project to your docker home directory.
COPY ./requirements.txt /usr/src/seiketsu

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# run this command to install all dependencies
RUN pip install -r requirements.txt

# copy whole project to your docker home directory.
COPY . /usr/src/seiketsu

# port where the Django app runs
EXPOSE 8080

# copy entry point script
COPY docker-entrypoint.sh /

# make it executable
RUN ["chmod", "+x", "docker-entrypoint.sh"]

# run entrypoint script
ENTRYPOINT ["/bin/bash", "-c", "./docker-entrypoint.sh"]