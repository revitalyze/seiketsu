#!/bin/bash

# Apply database migrations
echo "Generating migrations"
python ./seiketsu/manage.py makemigrations

# Apply database migrations
echo "Apply database migrations"
python ./seiketsu/manage.py migrate

if [ "$DJANGO_SUPERUSER_USERNAME" ]
then
    python ./seiketsu/manage.py createsuperuser \
        --noinput \
        --username $DJANGO_SUPERUSER_USERNAME \
        --email $DJANGO_SUPERUSER_USERNAME
fi

# Start server
echo "Starting server"
python ./seiketsu/manage.py runserver 0.0.0.0:$PORT