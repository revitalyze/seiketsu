from pprint import pprint
import csv
import requests
from datetime import datetime
from Dash.models import Materialien_AT, Materialien_EU, gln_Data


class material_importer:
    def austrian_codex():
        try:
            local_file = 'local_copy_afallkodex.csv'
            remote_url = 'https://secure.umweltbundesamt.at/edm_portal/redaList.do?seqCode=c2ck5gyutbw7qf&6578706f7274=1&entireLsq=true&d-49520-e=1'
            data = requests.get(remote_url)
            with open(local_file, 'wb')as file:
                file.write(data.content)

        except:
            print("something went wrong with the import of the austrian Abfallkodex")

        try:
            Materialien_AT.objects.all().delete()
        except:
            pass
        try:
            with open("local_copy_afallkodex.csv", newline='\n', encoding='utf-8') as f:
                for l in csv.reader(f, escapechar='\\'):
                    counter = 0
                    material = Materialien_AT()
                    for p in l:
                        if counter == 0:
                            if p:
                                material.Gueltigkeitsbeginn = datetime.fromisoformat(
                                    p)
                        elif counter == 1:
                            if p:
                                material.Gueltigkeitsende = datetime.fromisoformat(
                                    p)
                        elif counter == 2:
                            if p:
                                material.GTIN = int(p)
                        elif counter == 3:
                            if p:
                                material.Abfallschluesselnummer = int(p)
                        elif counter == 4:
                            if p:
                                material.Abfallspezifizierung = int(p)
                        elif counter == 5:
                            if p:
                                material.Gefaehrlich = p
                        elif counter == 6:
                            if p:
                                material.Bezeichnung = p
                        elif counter == 7:
                            if p:
                                material.Beschreibung = p
                        elif counter == 8:
                            if p:
                                material.Schlüsselnummer_falls_gefaehrlich = p
                        elif counter == 9:
                            if p:
                                material.Schluesselnummer_falls_ausgestuft_bzw_nicht_gefaehrlich = p
                        elif counter == 10:
                            if p:
                                material.Anmerkung = p
                        elif counter == 11:
                            if p:
                                material.Rechtliches_Inkrafttreten = datetime.fromisoformat(
                                    p)
                        elif counter == 12:
                            if p:
                                material.Rechtliches_Ausserkrafttreten = datetime.fromisoformat(
                                    p)

                        counter += 1
                    #
                    material.save()

        except:
            print("file could not be read")

    def european_codex():
        try:
            print("ping")
            local_file = 'local_copy_afallkodex_eu.csv'
            remote_url = 'https://secure.umweltbundesamt.at/edm_portal/redaList.do?seqCode=ev7jv8yw2ndj9a&d-49520-e=1&6578706f7274=1'
            data = requests.get(remote_url)
            with open(local_file, 'wb')as file:
                file.write(data.content)

        except:
            print("something went wrong with the import of the european Abfallkodex")
        try:
            Materialien_EU.objects.all().delete()
        except:
            pass
        try:
            with open("local_copy_afallkodex_eu.csv", newline='\n', encoding='utf-8') as f:
                for l in csv.reader(f, escapechar='\\'):
                    counter = 0
                    material = Materialien_EU()
                    for p in l:
                        if counter == 0:
                            if p:
                                material.Abfallschluesselnummer = p.replace(
                                    " ", "")
                        elif counter == 1:
                            if p:
                                p = True
                            material.Gefaehrlich = p
                        elif counter == 2:
                            if '\xa0' in p:
                                p.replace(u'\xa0', u'')
                            material.Bezeichnung = p
                        counter += 1
                    material.save()
        except:
            print("file could not be read")
