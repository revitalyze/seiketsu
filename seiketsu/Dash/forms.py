from django import forms
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from .models import Deponie, Bauunternehmer, Opening_Hours, User_Adress, gis_Location, gln_Data, Country
from .models import Deponie, Bauunternehmer, User_Adress, gln_Data, Materialien_AT, gis_Location, Opening_Hours
from django.contrib.admin import widgets
from django_select2 import forms as s2forms
import django_select2

from .models import Country
from .models import Deponie, Bauunternehmer, User_Adress, gln_Data, gis_Location, Opening_Hours


class UserForm_Deponie(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username', 'email', 'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update(
            {'id': 'deponie_mail', 'placeholder': 'name@example.com'})
        self.fields['username'].widget.attrs.update({'id': 'deponie_username'})
        self.fields['password'].widget.attrs.update(
            {'id': 'deponie_password', 'name': 'password_deponie'})
        self.helper = FormHelper()
        self.helper.form_show_labels = False


class UserProfileInfo_Deponie(forms.ModelForm):
    class Meta():
        model = Deponie
        fields = ['uid', ]  # 'gln']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['uid'].widget.attrs.update({'id': 'deponie_uid'})
        self.helper = FormHelper()
        self.helper.form_show_labels = False


class UserForm_Bauunternehmer(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username', 'email', 'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update(
            {'id': 'bauunternehmer_mail', 'placeholder': 'name@example.com'})
        self.fields['username'].widget.attrs.update(
            {'id': 'bauunternehmer_username'})
        self.fields['password'].widget.attrs.update(
            {'id': 'bauunternehmer_password'})
        self.helper = FormHelper()
        self.helper.form_show_labels = False


class UserProfileInfo_Bauunternehmer(forms.ModelForm):
    class Meta():
        model = Bauunternehmer
        fields = ('uid',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['uid'].widget.attrs.update({'id': 'bauunternehmer_uid'})
        self.helper = FormHelper()
        self.helper.form_show_labels = False


class ChangeAdressData(forms.ModelForm):
    class Meta():
        model = User_Adress
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ChangeAdressData, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False


class SearchLandfills(forms.Form):
    countries = forms.ModelChoiceField(queryset=Country.objects.all())
    city = forms.CharField(required=True)
    zipCode = forms.IntegerField(required=True)
    street = forms.CharField(required=False)
    tags = forms.CharField(required=True)
    material_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    material_country = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(SearchLandfills, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False


class LandfillsSearchResult:
    def __init__(self, gln, email, y_coordinate, x_coordinate, city, street, distance, zip_code, companyName, min_weight):
        self.gln = gln
        self.city = city
        self.street = street
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.zip_code = zip_code
        self.email = email
        self.distance = distance
        self.min_weight = min_weight
        self.companyName=companyName



class MaterialienWidget(django_select2.forms.Select2MultipleWidget):
    max_results = 2,
    minimumInputLength: 3


class AddGLN(forms.ModelForm):

    class Meta():
        model = gln_Data
        fields = '__all__'
        widgets = {
            'material_at': django_select2.forms.Select2MultipleWidget,
            'material_eu': MaterialienWidget,
        }

    def __init__(self, *args, **kwargs):
        super(AddGLN, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        #self.fields['Materialien_EU'].initial = self.instacnce.Materialien_EU.all().values_list("id", flat=True)


class AddGeoLocation(forms.ModelForm):
    class Meta():
        model = gis_Location
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class AddOpeningHours(forms.ModelForm):
    class Meta():
        model = Opening_Hours
        exclude = ['weekday', 'location']
        widgets = {
            'opening_hours_am_from': forms.TimeInput(attrs={'type': 'time'}),
            'opening_hours_am_to': forms.TimeInput(attrs={'type': 'time'}),
            'opening_hours_pm_from': forms.TimeInput(attrs={'type': 'time'}),
            'opening_hours_pm_to': forms.TimeInput(attrs={'type': 'time'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['opening_hours_am_from'].widget.attrs.update(
            {'class': 'form-control timepicker'})
        self.fields['opening_hours_am_to'].widget.attrs.update(
            {'class': 'form-control timepicker'})
        self.fields['opening_hours_pm_from'].widget.attrs.update(
            {'class': 'form-control timepicker'})
        self.fields['opening_hours_pm_to'].widget.attrs.update(
            {'class': 'form-control timepicker'})
