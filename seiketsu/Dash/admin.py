from django.contrib import admin
from Dash.models import Materialien_AT
from django.apps import apps
from django.contrib.gis.admin import OSMGeoAdmin
from django.contrib.auth.models import User, Group

# Register your models here.
# admin.site.unregister(Group)
models = apps.get_models()


class Admin(OSMGeoAdmin):
    for model in models:
        try:
            admin.site.register(model)
        except admin.sites.AlreadyRegistered:
            pass
