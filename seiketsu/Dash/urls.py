from . import views
from django.urls import path, include

from django.contrib.auth import views as auth_views

app_name = 'Dash'

urlpatterns = [
    # path('loged_in', views.index),
    #path('', views.login),
    path('partner-werden/', views.partner_werden, name='partner_werden'),
    path('vision/', views.vision, name='vision'),
    path('blog/', views.blog_overview, name='blog_overview'), 
    path('blog_post01/', views.blog_post01, name='blog_post01'),
    path('blog_post02/', views.blog_post02, name='blog_post02'),
    path('blog_post03/', views.blog_post03, name='blog_post03'),
    path('blog_post04/', views.blog_post04, name='blog_post04'),
    path('blog_post05/', views.blog_post05, name='blog_post05'),   
    path('register/', views.register, name='register'),
    path('user_login/', views.user_login, name='user_login'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('remove_materials/', views.remove_materials, name="remove_materials"),
    path('user_profile/', views.user_profile, name='user_profile'),
    path('modify_user_profile/', views.modify_user_profile,
         name='modify_user_profile'),
    path('add_gln/', views.add_gln, name='add_gln'),
    path('change-password/', auth_views.PasswordChangeView.as_view(
        template_name='dash/dashboard/profile/change_password.html',
        success_url='/dash/user_profile'), name='change_password'),
    path("select2/", include("django_select2.urls")),
    path('nearby/', views.nearby, name="nearby"),
    path('admin/import_material_AT',
         views.import_material_AT, name='import_material_AT'),
    path('admin/import_material_EU',
         views.import_material_EU, name='import_material_EU'),
    #re_path(r'^jsi18n/$', 'django.views.i18n.javascript_catalog', name='jsi18n'),
    path('contact-landfill/', views.contact_landfill, name='contact-landfill'),
]
