class Home(generic.ListView):
    
    address = 'Shivaji Nagar, Bangalore, KA 560001'
    url = 'https://nominatim.openstreetmap.org/search/' + urllib.parse.quote(address) +'?format=json'
    response = requests.get(url).json()
    longitude = 11.728484 
    latitude = 47.354059
    user_location = Point(longitude, latitude, srid=4326)
    model = Deponie
    context_object_name = 'deponie'
    queryset = Deponie.objects.annotate(distance=Distance('location',
    user_location)
    ).order_by('distance')[0:6]
    template_name = 'dash/test.html'
    home=Home.as_view()