from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm
from .forms import AddGLN, AddOpeningHours, ChangeAdressData, UserForm_Deponie, UserProfileInfo_Deponie, \
    UserForm_Bauunternehmer, UserProfileInfo_Bauunternehmer, UserProfileInfo_Deponie, SearchLandfills, \
    LandfillsSearchResult
from django.http import JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
import django.contrib.messages as messages
from django import forms
from django.forms import formset_factory, modelformset_factory
from django.conf import settings
from django.views import generic
from django.contrib.gis.geos import fromstr
from django.contrib.gis.db.models.functions import Distance
from .models import Country, Deponie, Bauunternehmer, Materialien_AT, Materialien_EU, Opening_Hours, User_Adress, gis_Location, gln_Data
from django.contrib.auth.models import User, Group
from django.contrib.gis.geos.point import Point
import requests
import urllib.parse
from Dash.functions.parser_abfallkodex_austria import material_importer
from django.core.mail import BadHeaderError
from django.core import mail
from django.core.mail import EmailMultiAlternatives, send_mail

from pprint import pprint

DASH_NEARBY_HTML = 'dash/nearby.html'
CONTACT_LANDFILL_HTML = 'dash/contact-landfill.html'


class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = forms.EmailField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': '', 'id': 'hello'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': '',
            'id': 'hi',
        }
    ))


@login_required
def special(request):
    return HttpResponse("you are logged in")


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('user_login'))


def index(request):
    return render(request, 'dash/index.html')

def partner_werden(request):
    return render(request, 'dash/partner-werden.html')

def vision(request):
    return render(request, 'dash/vision.html')

def blog_overview(request):
    return render(request, 'dash/blog/blog_overview.html')

#---single blog posts, need to be done dynamically later ---#

def blog_post01(request):
    return render(request, 'dash/blog/blog_post1.html')
def blog_post02(request):
    return render(request, 'dash/blog/blog_post2.html')
def blog_post03(request):
    return render(request, 'dash/blog/blog_post3.html')
def blog_post04(request):
    return render(request, 'dash/blog/blog_post4.html')
def blog_post05(request):
    return render(request, 'dash/blog/blog_post5.html')

def register(request):
    registered = False
    user_form_deponie = UserForm_Deponie()
    profile_form_deponie = UserProfileInfo_Deponie()
    user_form_bauunternehmer = UserForm_Bauunternehmer()
    profile_form_bauunternehmer = UserProfileInfo_Bauunternehmer()

    if request.method == "POST":
        account_type = request.POST.get('account_type')
        if account_type == 'abfallbehandler':
            user_form_deponie = UserForm_Deponie(data=request.POST)
            profile_form_deponie = UserProfileInfo_Deponie(data=request.POST)

            if user_form_deponie.is_valid() and profile_form_deponie.is_valid():
                user = user_form_deponie.save()
                user.set_password(user.password)
                add_to_group = Group.objects.get(name='Abfallbehandler')
                add_to_group.user_set.add(user)
                user.save()
                profile = profile_form_deponie.save(commit=False)
                profile.user = user
                profile.save()
                registered = True
            else:
                print(user_form_deponie.errors, profile_form_deponie.errors)

        elif account_type == 'bauunternehmer':
            user_form_bauunternehmer = UserForm_Bauunternehmer(
                data=request.POST)
            profile_form_bauunternehmer = UserProfileInfo_Bauunternehmer(
                data=request.POST)

            if user_form_bauunternehmer.is_valid() and profile_form_bauunternehmer.is_valid():
                user = user_form_bauunternehmer.save()
                user.set_password(user.password)
                add_to_group = Group.objects.get(name='Bauunternehmer')
                add_to_group.user_set.add(user)
                user.save()
                profile = profile_form_bauunternehmer.save(commit=False)
                profile.user = user
                profile.save()
                registered = True
            else:
                print(user_form_bauunternehmer.errors,
                      profile_form_bauunternehmer.errors)

    return render(request, 'dash/registration.html', {'user_form_deponie': user_form_deponie,
                                                      'profile_form_deponie': profile_form_deponie,
                                                      'user_form_bauunternehmer': user_form_bauunternehmer,
                                                      'profile_form_bauunternehmer': profile_form_bauunternehmer,
                                                      'registered': registered})


def user_login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/dash/dashboard')

            else:
                return HttpResponse("UPPPSSS Something went wrong")
        else:
            print("someone tried to do the old hackedy hack")
            print("Username: {}".format(username))
            return HttpResponse("Computer says no!")

    else:
        return render(request, 'dash/login.html', {})


@login_required
def dashboard(request):
    return render(request, 'dash/dashboard/dashboard.html', {})


@login_required
def user_profile(request):
    if request.method == "GET":
        user_model_data = User.objects.get(pk=request.user.id)
        user_models = [Bauunternehmer, Deponie]
        primary_adress = None
        for model in user_models:
            try:
                current_user = model.objects.get(user=request.user.id)

                if current_user:
                    break
            except:
                messages.error(request, "User Data could not be found")
        try:
            user_adress_data = User_Adress.objects.all().filter(user=request.user.id)
            for adress_item in user_adress_data:
                if adress_item.is_primary_adress is True:
                    primary_adress = adress_item
                    break
        except:
            primary_adress = None

        try:
            user_gln_data = gln_Data.objects.all().filter(user=request.user.id)
            gln_data_container = []
            for data_item in user_gln_data:
                gln_data_container.append(data_item)
        except:
            gln_data_container = None

    if request.method == "POST":

        if request.POST['delete_gln']:
            gln_to_delete = gln_Data.objects.get(id=request.POST['delete_gln'])
            opening_days_to_delete = Opening_Hours.objects.all().filter(
                location=gln_to_delete.location)
            adress_to_delete = User_Adress.objects.get(
                id=gln_to_delete.adress.id)
            gis_location_to_delete = gis_Location.objects.get(
                id=gln_to_delete.location.id)
            gln_to_delete.delete()
            opening_days_to_delete.delete()
            adress_to_delete.delete()
            gis_location_to_delete.delete()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    return render(request, 'dash/dashboard/profile/user_profile.html',
                  {'usermodel_data': user_model_data, 'usermodel_type_data': current_user,
                   'usermodel_adress_data': primary_adress, 'usermodel_gln_data': gln_data_container, })


@login_required
def modify_user_profile(request):
    if request.method == "GET":
        try:
            user_adress_data = User_Adress.objects.all().filter(user=request.user.id)
            user_model_data = User.objects.get(pk=request.user.id)
            for adress_item in user_adress_data:
                if adress_item.is_primary_adress is True:
                    primary_adress = adress_item
                    break
            update_adress_form = ChangeAdressData(instance=primary_adress)
        except:
            update_adress_form = ChangeAdressData()

    if request.method == "POST":
        # user_adress = User_Adress.objects.get(user=)
        user_adress_form = ChangeAdressData(data=request.POST)
        user_model_data = User.objects.get(pk=request.user.id)
        if user_adress_form.is_valid():
            current_adress_form = user_adress_form.save(commit=False)
            current_adress_form.is_primary_adress = True
            current_adress_form.user = request.user
            current_adress_form.save()
            user_model_data.first_name=request.POST['company_name']
            user_model_data.save()
           
        return HttpResponseRedirect('/dash/user_profile')
    
    

    return render(request, 'dash/dashboard/profile/modify_user_profile.html', {'adress_form': update_adress_form, 'user_model_data':user_model_data})


@login_required
def remove_materials(request):
    material_ids_to_remove = request.POST.getlist('materials_to_remove[]')
    gln_to_remove_from = gln_Data.objects.get(id=request.POST.get('gln_id'))
    for material_id in material_ids_to_remove:
        split_id_from_country = material_id.split("_")
        if split_id_from_country[1] == "AT":
            material = Materialien_AT.objects.get(id=int(split_id_from_country[0]))
            gln_to_remove_from.material_at.remove(material)
        elif split_id_from_country[1] == "EU":
            material = Materialien_EU.objects.get(id=int(split_id_from_country[0]))
            gln_to_remove_from.material_eu.remove(material)

    return JsonResponse({"instance": material_ids_to_remove}, status=200)


@login_required
def add_gln(request):
    extra_forms = 7
    data = {
        'form-TOTAL_FORMS': '7',
        'form-INITIAL_FORMS': '7',
    }

    if request.method == "GET":

        if request.GET.get('gln-id', False):
            try:
                at_material = Materialien_AT.objects.all()
                eu_material = Materialien_EU.objects.all()
                opening_hours_form = modelformset_factory(
                    Opening_Hours, AddOpeningHours, extra=extra_forms, )
                opening_hours_formset = opening_hours_form(
                    data, queryset=Opening_Hours.objects.none())
                context_gln = request.GET['gln-id']
                gln_to_modify = gln_Data.objects.get(id=context_gln)
                gln_data_for_form = {'id': gln_to_modify.id, 'gln': gln_to_modify.gln,
                                     'min_amount_in_t': gln_to_modify.min_amount_in_t,
                                     'adress': gln_to_modify.adress, 'location': gln_to_modify.location,
                                     'user': gln_to_modify.user}
                gln_form = AddGLN(initial=gln_data_for_form)

                adress_to_modify = User_Adress.objects.get(
                    id=gln_to_modify.adress.id)
                adress_data_for_form = {'id': adress_to_modify.id,
                                        'is_primary_adress': adress_to_modify.is_primary_adress,
                                        'user': adress_to_modify.user, 'zipCode': adress_to_modify.zipCode,
                                        'city': adress_to_modify.city, 'street': adress_to_modify.street,
                                        'country': adress_to_modify.country, 'phone': adress_to_modify.phone}
                empty_adress_form = ChangeAdressData(
                    initial=adress_data_for_form)
                opening_days_to_modify = Opening_Hours.objects.all().filter(
                    location=gln_to_modify.location)
                opening_hours_form_to_modify = modelformset_factory(
                    Opening_Hours, AddOpeningHours, extra=extra_forms)
                opening_hours_formset_to_modify = opening_hours_form_to_modify(
                    queryset=opening_days_to_modify)

                database_material_at = gln_to_modify.material_at.all()
                database_material_eu = gln_to_modify.material_eu.all()

                return render(request, 'dash/dashboard/profile/modify_gln.html',
                              {'gln_form': gln_form,
                               'adress_form_gln': empty_adress_form,
                               'hours_form': opening_hours_formset_to_modify,
                               'gln_id': context_gln,
                               'database_material_at': database_material_at,
                               'database_material_eu': database_material_eu,
                               'material_at': at_material,
                               'material_eu': eu_material})

            except:
                pass

        else:

            try:
                at_material = Materialien_AT.objects.all()
                eu_material = Materialien_EU.objects.all()
                opening_hours_form = formset_factory(
                    AddOpeningHours, extra=extra_forms)
                opening_hours_formset = opening_hours_form(data)
                empty_gln_form = AddGLN()
                empty_adress_form = ChangeAdressData()
                return render(request, 'dash/dashboard/profile/add_gln.html',
                              {'gln_form': empty_gln_form,
                               'adress_form_gln': empty_adress_form,
                               'hours_form': opening_hours_formset,
                               'material_at': at_material,
                               'material_eu': eu_material})

            except:
                messages.error(request, "Adress Data could not be found")
                return HttpResponse('<h1>Page was found</h1>')

    if request.method == "POST":
        try:
            opening_hours_form = formset_factory(
                AddOpeningHours, extra=extra_forms)
            gln_form = AddGLN(request.POST)
            adress_form = ChangeAdressData(request.POST)
            try:
                opening_hours_all = opening_hours_form(request.POST)
            except:
                pass

            if gln_form.is_valid() and adress_form.is_valid():

                if request.POST.get('gln_id', False):

                    gln_data = gln_Data.objects.get(id=request.POST['gln_id'])
                    adress_data = User_Adress.objects.get(
                        id=gln_data.adress.id)
                    gis_location = gis_Location.objects.get(
                        id=gln_data.location.id)
                else:
                    adress_data = adress_form.save(commit=False)
                    gln_data = gln_form.save(commit=False)
                    adress_data.user = request.user
                    gln_data.user = request.user
                    gis_location = gis_Location()
                try:
                    adress_string_location = adress_data.street + ' ' + adress_data.city + \
                                             ' ' + str(adress_data.zipCode) + ' ' + adress_data.city
                    url = 'https://nominatim.openstreetmap.org/search/' + \
                          urllib.parse.quote(
                              adress_string_location) + '?format=json'
                    response = requests.get(url).json()
                    gis_location.location = Point(
                        float(response[0]['lon']), float(response[0]['lat']))
                    gis_location.user = request.user

                    gis_location.save()
                    gln_data.location = gis_location
                    gln_data.adress = adress_data

                    gln_data.gln = request.POST['gln']
                    gln_data.min_amount_in_t = request.POST['min_amount_in_t']
                    gln_data.location = gis_location
                    adress_data.save()
                    try:
                        gln_data.save()
                        gln_material_container = request.POST['materials-for-gln'].split(',')
                        material_country = request.POST['codex-radio']
                        if material_country == "AT-codex":
                            for material in gln_material_container:
                                current_material = Materialien_AT.objects.get(id=int(material))
                                gln_data_material = gln_data.material_at.all()
                                if current_material in gln_data_material:
                                    pass
                                else:
                                    gln_data.material_at.add(current_material)
                        elif material_country == "EU-codex":
                            for material in gln_material_container:
                                current_material = Materialien_EU.objects.get(id=int(material))
                                gln_data_material = gln_data.material_eu.all()
                                if current_material in gln_data_material:
                                    pass
                                else:
                                    gln_data.material_eu.add(current_material)
                    except:
                        pass

                    gln_data.save()

                    if request.POST.get('gln_id', False):
                        opening_hours_form_to_modify = modelformset_factory(Opening_Hours, AddOpeningHours, extra=(
                                (extra_forms - len(opening_hours_all)) + len(opening_hours_all)))
                        opening_hours_all = opening_hours_form_to_modify(
                            request.POST)

                        if opening_hours_all.is_valid():
                            pass
                        else:
                            return HttpResponse('<h1>Form not valid</h1>')

                    for f in opening_hours_all:
                        day = Opening_Hours()
                        if f.prefix == "form-0":
                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 0
                                    day.location = gis_location
                                    day.save()

                        elif f.prefix == "form-1":
                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 1
                                    day.location = gis_location
                                    day.save()

                        elif f.prefix == "form-2":

                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 2
                                    day.location = gis_location
                                    day.save()

                        elif f.prefix == "form-3":
                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 3
                                    day.location = gis_location
                                    day.save()

                        elif f.prefix == "form-4":
                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 4
                                    day.location = gis_location
                                    day.save()

                        elif f.prefix == "form-5":
                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 5
                                    day.location = gis_location
                                    day.save()

                        elif f.prefix == "form-6":
                            if f.is_valid() and bool(f.clean()):
                                if f.cleaned_data['opening_hours_am_from'] is None and f.cleaned_data[
                                    'opening_hours_pm_from'] is None:
                                    continue
                                else:
                                    try:
                                        if f.cleaned_data['id']:
                                            day = f.cleaned_data['id']
                                    except:
                                        day = Opening_Hours()
                                    day.opening_hours_am_from = f.cleaned_data['opening_hours_am_from']
                                    day.opening_hours_am_to = f.cleaned_data['opening_hours_am_to']
                                    day.opening_hours_pm_from = f.cleaned_data['opening_hours_pm_from']
                                    day.opening_hours_pm_to = f.cleaned_data['opening_hours_pm_to']
                                    day.weekday = 6
                                    day.location = gis_location
                                    day.save()

                except:
                    messages.error(request, "Adress Data could not be found")
                    return HttpResponse('<h1>Page was not found</h1>')

                return HttpResponseRedirect('/dash/user_profile')
        except:
            messages.error(request, "Adress Data could not be found")
            return HttpResponse('<h1>Page was found</h1>')


def nearby(request):
    at_material = Materialien_AT.objects.all()
    eu_material = Materialien_EU.objects.all()
    search_result = []

    if request.method == "GET":
        empty_search_form = SearchLandfills()
        return render(request, DASH_NEARBY_HTML,
                      {'search_landfills': empty_search_form, 'material_at': at_material, 'material_eu': eu_material})
    if request.method == "POST":
        search_form = SearchLandfills(request.POST)
        if search_form.is_valid():
            try:
                location = search_form.cleaned_data["street"] + ' ' + search_form.cleaned_data["city"] + ' ' + str(
                    search_form.cleaned_data["zipCode"]) + ' ' + search_form.cleaned_data["countries"].name
                url = 'https://nominatim.openstreetmap.org/search/' + urllib.parse.quote(location) + '?format=json'
                response = requests.get(url).json()
                requester_location = Point(float(response[0]['lon']), float(response[0]['lat']), srid=4326)
                locations = gis_Location.objects.annotate(distance=Distance('location', requester_location)).order_by(
                    'distance')[0:6]

                material_id = search_form.cleaned_data["material_id"]
                material_country = search_form.cleaned_data["material_country"]
            
                if material_country == "EU":
                    material = Materialien_EU.objects.get(id=material_id)
                else:
                    material = Materialien_AT.objects.get(id=material_id)
                landfill_locations_distance = []
                for loc in locations.all():
                    if material_country == "EU":
                        gln_data = gln_Data.objects.filter(material_eu=material, location=loc)
                    else:
                        gln_data = gln_Data.objects.filter(material_at=material, location=loc)

                    for gln in gln_data.all():
                       
                        search_result.append(
                            LandfillsSearchResult(gln=gln.id, distance=round(loc.distance.km, 2), email=gln.adress.user.email, companyName=gln.user.first_name, city=gln.adress.city,
                                                  zip_code=gln.adress.zipCode, street=gln.adress.street, x_coordinate =gln.location.location.x, y_coordinate =gln.location.location.y,
                                                  min_weight=gln.min_amount_in_t))

                return render(request, DASH_NEARBY_HTML,
                              {'search_results': search_result, 'search_landfills': search_form,
                               'material_at': at_material, 'material_eu': eu_material})

            except Exception as e:
                messages.error(e, "An error occurred!")
                return render(request, DASH_NEARBY_HTML,
                              {'search_results': search_result, 'search_landfills': search_form,
                               'material_at': at_material, 'material_eu': eu_material})


def import_material_AT(request):
    material_importer.austrian_codex()
    return HttpResponseRedirect('/admin/')


def import_material_EU(request):
    material_importer.european_codex()
    return HttpResponseRedirect('/admin/')


def contact_landfill(request):
    if request.method == "POST":
        subject = request.POST.get('subject')
        message = request.POST.get('message')
        sender = request.POST.get('email')
        gln = request.POST.get('gln')
        if request.FILES['contact-mail-attachment']:
            try:
                attachement = request.FILES['contact-mail-attachment'].read()      
            except:
                pass
        gln = gln_Data.objects.get(id=gln)
        connection = mail.get_connection()
        delivery_option= request.POST['radio-delivery-variant']
        html_content = '<p>Sie haben eine Nachricht von '+sender+'</p><br><p>'+message+'</p>'

        body = {
            'message': "Sie haben eine Nachricht von "+ sender +'\n\n' + message
        }  
        message = "\n".join(body.values())
        
        if delivery_option == "pickup":
            message = message + "\n\nTransportinformation: Es wird um eine Abholung des Materials gebeten"
            html_content = html_content + '<br><p><b>Transportinformation: Es wird um eine Abholung des Materials gebeten</b></p>'
        else:
            message = message + "\n\nTransportinformation: Das Material wird selbst angeliefert"
            html_content = html_content + '<br><p><b>Transportinformation: Das Material wird selbst angeliefert</b></p>'
        
        
        try:
            connection.open()
            #headers = {'Reply-To': sender}
            msg = EmailMultiAlternatives(subject, message, "Revitalyze <noreply@revitalyze.io>",
                                         bcc=[sender],  connection=connection)
            #cmsg.content_subtype = "text/html"
            msg.attach('Anhang1', attachement, 'image/jpeg')
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            connection.close()
           # send_mail(subject, message, settings.EMAIL_HOST_USER,  ['patrick.goessl@gmail.com'],)
        except BadHeaderError:
            return HttpResponseBadRequest('Invalid header found.')
        return HttpResponse('/')


class Home(generic.ListView):
    address = 'Erbstollenweg 9, 6130 Schwaz, Austria'
    url = 'https://nominatim.openstreetmap.org/search/' + \
          urllib.parse.quote(address) + '?format=json'
    response = requests.get(url).json()
    longitude = 11.728484
    latitude = 47.354059

    user_location = Point(longitude, latitude, srid=4326)
    model = gln_Data
    context_object_name = 'deponie'
    queryset = gis_Location.objects.annotate(distance=Distance('location',
                                                               user_location)
                                             ).order_by('distance')[0:6]
    template_name = 'dash/test.html'
