from django.db import models
from django.db.models.deletion import PROTECT
from django.db.models.fields.related import ForeignKey
from phonenumber_field.modelfields import PhoneNumberField
from djmoney.models.fields import MoneyField
from django.contrib.auth.models import User
from django.contrib.gis.db import models as gisModels
from django.utils.translation import gettext_lazy as _

# Create your models here.

class DeponieTyp(models.Model):
    name=models.CharField(max_length=25)

class Sonderbedingungen(models.Model):
    name= models.CharField(max_length=256, null=True, blank=True)
    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=256)
    phoneCode = models.IntegerField()
    def __str__(self):
        return self.name

class Deponie(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True) 
    uid = models.CharField(max_length=265, null=True)    
    deponieTyp = models.ManyToManyField(DeponieTyp, blank=True)
    def __str__(self):
        return super().__str__()

class Preisliste(models.Model):
    preisProTonne = MoneyField(max_digits=14, decimal_places=2, default_currency='EUR')
    deponie = models.ForeignKey(Deponie, on_delete=models.CASCADE)


class Bauunternehmer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    uid = models.CharField(max_length=20, null=True)   

    def __str__(self):
        return super().__str__()
    
class Bill(models.Model):
    billNumber = models.IntegerField()
    deponie= models.ForeignKey(Deponie, on_delete=models.PROTECT)
    bauunternehmer = models.ForeignKey(Bauunternehmer, on_delete=models.PROTECT)

class User_Adress(models.Model):
    is_primary_adress = models.BooleanField(default=False)
    user=models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    zipCode=models.IntegerField(null=True)
    city=models.CharField(max_length=256, null=True)
    street=models.CharField(max_length=256, null=True)
    country=models.ForeignKey(Country, on_delete=models.PROTECT, null=True)
    phone=PhoneNumberField(null=True, blank=True)
    
    def __str__(self):
        return "%s %s %s" %(self.street, self.city, self.country) 
       
class gis_Location(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)   
    location = gisModels.PointField(null=True)     
    def __str__(self):
        return str(self.location)
        
class Opening_Hours(models.Model):
    WEEKDAYS = [
        (0, _("Monday")),
        (1, _("Tuesday")),
        (2, _("Wednesday")),
        (3, _("Thursday")),
        (4, _("Friday")),
        (5, _("Saturday")),
        (6, _("Sunday")),
    ]
    weekday = models.PositiveSmallIntegerField(choices=WEEKDAYS)
    location = models.ForeignKey(gis_Location, on_delete=models.CASCADE, null=True)
    opening_hours_am_from = models.TimeField(null=True, blank=True)
    opening_hours_am_to = models.TimeField(null=True, blank=True)
    opening_hours_pm_from = models.TimeField( null=True, blank=True)  
    opening_hours_pm_to = models.TimeField(null=True, blank=True)
       
    def __str__(self):
        return str(self.WEEKDAYS[self.weekday])
    
class gln_Data(models.Model): #standort an dem müll abgeladen werden kann
    gln = models. BigIntegerField(null=True)
    user=models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    location = models.ForeignKey(gis_Location, on_delete=models.CASCADE, null=True, blank=True)
    adress = ForeignKey(User_Adress, models.SET_NULL, null=True, blank=True)
    min_amount_in_t = models.IntegerField(null=True)
    special_conditions = models.ManyToManyField(Sonderbedingungen, blank=True)
    def __str__(self):
        return str(self.gln)
class Order(models.Model):
    date=models.DateTimeField()
    deponie= models.ForeignKey(Deponie, on_delete=models.PROTECT)
    bauunternehmer = models.ForeignKey(Bauunternehmer, on_delete=models.PROTECT) 
    bill_nr= models.ForeignKey(Bill, on_delete=models.PROTECT)
    def __str__(self):
        return str(self.bill_nr)
class Materialien_AT(models.Model):
    #fields of the austrian garbage-matrix
    Gueltigkeitsbeginn = models.DateTimeField(null=True, blank=True)
    Gueltigkeitsende = models.DateTimeField(null=True, blank=True)
    GTIN= models.BigIntegerField(null=True, blank=True)
    Abfallspezifizierung = models.IntegerField(null=True, blank=True)
    Abfallschluesselnummer = models.IntegerField(null=True, blank=True)
    Gefaehrlich = models.CharField(max_length=256, null=True)
    Bezeichnung= models.TextField(null=True, blank=True)
    Beschreibung = models.TextField(null=True, blank=True)
    Schlüsselnummer_falls_gefaehrlich = models.TextField(null=True, blank=True)
    Schluesselnummer_falls_ausgestuft_bzw_nicht_gefaehrlich = models.TextField(null=True, blank=True)
    Anmerkung = models.TextField(null=True, blank=True)
    Rechtliches_Inkrafttreten = models.DateTimeField(null=True, blank=True)
    Rechtliches_Ausserkrafttreten = models.DateTimeField(null=True, blank=True)
    #relationship fields
    standort = models.ManyToManyField(gln_Data, related_name="Materialien_AT", symmetrical=False, blank=True)
    preisliste = models.ManyToManyField(Preisliste, related_name="Materialien_AT", symmetrical=False, blank=True)
    order = models.ManyToManyField(Order, related_name="Materialien_AT", symmetrical=False, blank=True)
    
class Materialien_EU(models.Model):
    #fields of the european garbage-matrix
    Abfallschluesselnummer= models.TextField(null=True, blank=True)
    Gefaehrlich = models.BooleanField(blank=True, null=True)
    Bezeichnung= models.TextField(null=True, blank=True)
    #relationship fields
    standort = models.ManyToManyField(gln_Data, related_name="Materialien_EU", symmetrical=False, blank=True)
    preisliste = models.ManyToManyField(Preisliste, related_name="Materialien_EU", symmetrical=False, blank=True)
    order = models.ManyToManyField(Order, related_name="Materialien_EU", symmetrical=False, blank=True)
    