from django.db import models
from django.db.models.deletion import PROTECT
from django.db.models.fields.related import ForeignKey
from phonenumber_field.modelfields import PhoneNumberField
from djmoney.models.fields import MoneyField
from django.contrib.auth.models import User
from django.contrib.gis.db import models as gisModels
from django.utils.translation import gettext_lazy as _

# Create your models here.

class DeponieTyp(models.Model):
    name=models.CharField(max_length=25)

class Materialien(models.Model):
    gtin=models.IntegerField()
    keyNumber = models.IntegerField()
    name = models.CharField(max_length=256)
    spezifizierung = models.CharField(max_length=256, blank=True)

class Sonderbedingungen(models.Model):
    name= models.CharField(max_length=256, null=True, blank=True)
    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=256)
    phoneCode = models.IntegerField()
    def __str__(self):
        return self.name

class Deponie(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True) 
    uid = models.CharField(max_length=265, null=True)
    materialien = models.ManyToManyField(Materialien, blank=True)
    deponieTyp = models.ManyToManyField(DeponieTyp, blank=True)
    def __str__(self):
        return super().__str__()

class Preisliste(models.Model):
    preisProTonne = MoneyField(max_digits=14, decimal_places=2, default_currency='EUR')
    deponie = models.ForeignKey(Deponie, on_delete=models.CASCADE)
    material = models.ForeignKey(Materialien, on_delete=models.CASCADE)

class Bauunternehmer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    uid = models.CharField(max_length=20, null=True)   

    def __str__(self):
        return super().__str__()
    
class Bill(models.Model):
    billNumber = models.IntegerField()
    deponie= models.ForeignKey(Deponie, on_delete=models.PROTECT)
    bauunternehmer = models.ForeignKey(Bauunternehmer, on_delete=models.PROTECT)
      
class Order(models.Model):
    date=models.DateTimeField()
    deponie= models.ForeignKey(Deponie, on_delete=models.PROTECT)
    material = models.ForeignKey(Materialien, on_delete=models.PROTECT)
    bauunternehmer = models.ForeignKey(Bauunternehmer, on_delete=models.PROTECT) 
    bill_nr= models.ForeignKey(Bill, on_delete=models.PROTECT)

class User_Adress(models.Model):
    is_primary_adress = models.BooleanField(default=False)
    user=models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    zipCode=models.IntegerField(null=True)
    city=models.CharField(max_length=256, null=True)
    street=models.CharField(max_length=256, null=True)
    country=models.ForeignKey(Country, on_delete=models.PROTECT, null=True)
    phone=PhoneNumberField(null=True, blank=True)
    
    def __str__(self):
        return "%s %s %s" %(self.street, self.city, self.country) 
       
class Opening_Hours(models.Model):
    opening_hours_am_from = models.TimeField(null=True, blank=True)
    opening_hours_am_to = models.TimeField(null=True, blank=True)
    opening_hours_pm_from = models.TimeField( null=True, blank=True)  
    opening_hours_pm_to = models.TimeField(null=True, blank=True)
    
class gis_Location(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)   
    location = gisModels.PointField(null=True)     
    def __str__(self):
        return str(self.location)
        
class Opening_Days(models.Model):
    WEEKDAYS = [
        (0, _("Monday")),
        (1, _("Tuesday")),
        (2, _("Wednesday")),
        (3, _("Thursday")),
        (4, _("Friday")),
        (5, _("Saturday")),
        (6, _("Sunday")),
    ]
    weekday = models.PositiveSmallIntegerField(choices=WEEKDAYS)
    hours = models.ForeignKey(Opening_Hours, on_delete=models.CASCADE, null=True, blank=True)
    location = models.ForeignKey(gis_Location, on_delete=models.CASCADE, null=True)
       
    def __str__(self):
        return str(self.WEEKDAYS[self.weekday])
    
class gln_Data(models.Model):
    gln = models.IntegerField(null=True)
    user=models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    location = models.ForeignKey(gis_Location, on_delete=models.CASCADE, null=True, blank=True)
    adress = ForeignKey(User_Adress, models.SET_NULL, null=True, blank=True)
    min_amount_in_t = models.IntegerField(null=True)
    special_conditions = models.ManyToManyField(Sonderbedingungen, blank=True)