# Generated by Django 3.2.5 on 2021-11-01 19:32

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Dash', '0005_auto_20211020_1937'),
    ]

    operations = [
        migrations.AddField(
            model_name='bauunternehmer',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(null=True, srid=4326),
        ),
        migrations.AddField(
            model_name='deponie',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(null=True, srid=4326),
        ),
        migrations.AlterField(
            model_name='bauunternehmer',
            name='order',
            field=models.ManyToManyField(to='Dash.Order'),
        ),
    ]
