# Generated by Django 3.2.5 on 2022-01-05 19:06

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('Dash', '0002_gln_data_adress'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user_adress',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None),
        ),
    ]
