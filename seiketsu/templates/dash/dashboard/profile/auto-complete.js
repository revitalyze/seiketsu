$(document).ready(function() {
    var availableTags = [
        { % for material in material_eu % }
        "Bezeichnung (EU): " + "{{material.Bezeichnung}} " + "Schlüsselnummer (EU): " + "{{material.Abfallschluesselnummer}}",
        { % endfor % } { % for material in material_at % }
        "Bezeichnung (AT): " + "{{material.Bezeichnung}} " + "Schlüsselnummer (AT): " + "{{material.Abfallschluesselnummer}}",
        { % endfor % }
    ];
    $("#tags").autocomplete({
        minLength: 3,
        source: availableTags,

    });
});



function extractLast(term) {
    return split(term).pop();
}

function bindAutoComplete(ele, options) {
    var text = ele.val();
    text = text == null || text == undefined ? "" : text;
    $(ele).autocomplete(options).data("autocomplete")._renderItem = function(ul, item) {
        var checked = (text.indexOf(item.label + ', ') > -1 ? 'checked' : '');
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append('<a href="javascript:;"><input type="checkbox"' + checked + '/>' + item.label + '</a>')
            .appendTo(ul);
    };
}
$(function() {
        var $this;
        var multiSelectOptions = {
            minLength: 0,
            source: function(request, response) {
                response($.map(data, function(item) {

                    return {
                        label: item
                    }
                }));
            },
            focus: function() {
                // prevent value inserted on focus
                //$($this).autocomplete("search");
                return false;
            },
            select: function(event, ui) {
                var text = $this.val();
                text = text == null || text == undefined ? "" : text;
                var checked = (text.indexOf(ui.item.value + ', ') > -1 ? 'checked' : '');
                if (checked == 'checked') {
                    this.value = this.value.replace(ui.item.value + ', ', '')
                } else {
                    var terms = split(this.value);
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push(ui.item.value);
                    // add placeholder to get the comma-and-space at the end
                    terms.push("");
                    this.value = terms.join(", ");
                }
                return false;
            },
            open: function() {
                $("ul.ui-menu").width($(this).innerWidth());
            }
        }
        $(document).find('textarea[class*="multiselect-element"]').live('keydown', function() {
            bindAutoComplete($this, multiSelectOptions);
        }).live('focus', function() {
            $this = $(this);
            var text = $this.val();
            bindAutoComplete($this, multiSelectOptions);
            $($this).autocomplete("search");
        })
    }) <
    /script>