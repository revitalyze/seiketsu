let map, infoWindow, current_lat, current_long;


function initMap() {
    const mainLocation = location_points[0].toString().split(' ')
    const mainLocationLat = parseFloat(mainLocation[1])
    const mainLocationLng = parseFloat(mainLocation[0])
    const myLatLng = { lat: mainLocationLat, lng: mainLocationLng };
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 10,
        mapId: 'aab9c48d2d58a696',
        center: myLatLng,
    });

    var markerLatLngs = [];

    for (let counter = 0; counter < location_points.length; counter++) {
        let location = location_points[counter].toString().split(' ');
        const pointLat = parseFloat(location[1]);
        const pointLng = parseFloat(location[0]);
        markerLatLngs.push(new google.maps.LatLng(pointLat, pointLng))
        const pointLatLng = { lat: pointLat, lng: pointLng };
        new google.maps.Marker({
            position: pointLatLng,
            map,
            title: nameForMarker,
        });

    }

    var latlngbounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markerLatLngs.length; i++) {
        latlngbounds.extend(markerLatLngs[i]);
    }
    map.fitBounds(latlngbounds);

}

function nearbyLandfills(landfillMarkers) {
    var markerLatLngs = landfillMarkers;

    for (let counter = 0; counter < location_points.length; counter++) {
        let location = location_points[counter].toString().split(' ');
        const pointLat = parseFloat(location[1]);
        const pointLng = parseFloat(location[0]);
        markerLatLngs.push(new google.maps.LatLng(pointLat, pointLng))
        const pointLatLng = { lat: pointLat, lng: pointLng };
        new google.maps.Marker({
            position: pointLatLng,
            map,
            title: pointLat.toString(),
        });

    }

    var latlngbounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markerLatLngs.length; i++) {
        latlngbounds.extend(markerLatLngs[i]);
    }
    map.fitBounds(latlngbounds);
}