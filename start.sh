#!/bin/bash

cd seiketsu
source /home/patrick/.virtualenvs/seiketsu/bin/activate
sudo service docker start
sudo docker start postgis
python manage.py runserver
